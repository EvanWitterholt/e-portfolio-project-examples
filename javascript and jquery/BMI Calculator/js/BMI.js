"use strict";

var $ = function(id){
    return document.getElementById(id);
};

const MINWEIGHT = 1;
const MAXWEIGHT = 777;
const MINHEIGHT = 12;
const MAXHEIGHT = 96;

const MAXUNDER = 18.50;
const MAXOPTIMAL = 25.00;
const MAXOVER = 30.00;


var BMI = function(){

    var PoundWeight = parseInt(PoundWeight);
    var InchHeight = parseInt(InchHeight);
    var bmi = "";
    var bmiStatus = "";

    bmiStatus = $("status").value;
    InchHeight = $("height").value;
    PoundWeight = $("weight").value;

    if(isNaN(InchHeight)||(InchHeight>MAXHEIGHT)||(InchHeight<MINHEIGHT))
    {
        $("height").nextElementSibling.firstChild.nodeValue = "Numeric Input Between 12 And 96 Inches Is Required";
        validInput = false;
    }
    else
    {
        $("height").nextElementSibling.firstChild.nodeValue = "";
    }

    if(isNaN(PoundWeight)||(PoundWeight>MAXWEIGHT)||(PoundWeight<MINWEIGHT))
    {
        $("weight").nextElementSibling.firstChild.nodeValue = "Numeric Input Between 1 And 777 Pounds Is Required";
        validInput = false;
    }
    else
    {
        $("weight").nextElementSibling.firstChild.nodeValue = "";
    }

    
	if (bmi<UNDERWEIGHT) {

		bmiStatus = "Underweight";

		underweight++;
		
	}
	else if (bmi>=UNDERWEIGHT && bmi<OPTIMALWEIGHT) {

		bmiStatus = "Optimal Weight";

		optimalweight++;
		
	}
	else if(bmi>=OPTIMALWEIGHT && bmi<OVERWEIGHT){

		bmiStatus = "Overweight";

		overweight++;

	}
	else if(bmi>=OVERWEIGHT){

		bmiStatus = "Obese";

		obese++;

	}


}

window.onload = function(){
    $("calculate").onclick = processEntries;
    $("reset").onclick = resetTheForm;
    $("height").focus();
};