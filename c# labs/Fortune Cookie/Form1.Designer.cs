﻿namespace Chapter07_05
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonFortune = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxFortune = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonFortune
            // 
            this.buttonFortune.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonFortune.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonFortune.Image = ((System.Drawing.Image)(resources.GetObject("buttonFortune.Image")));
            this.buttonFortune.Location = new System.Drawing.Point(26, 30);
            this.buttonFortune.Margin = new System.Windows.Forms.Padding(0);
            this.buttonFortune.Name = "buttonFortune";
            this.buttonFortune.Size = new System.Drawing.Size(372, 321);
            this.buttonFortune.TabIndex = 0;
            this.buttonFortune.UseVisualStyleBackColor = false;
            this.buttonFortune.Click += new System.EventHandler(this.buttonFortune_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Franklin Gothic Medium", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(26, 391);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(372, 82);
            this.button1.TabIndex = 1;
            this.button1.Text = "Reset";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxFortune
            // 
            this.textBoxFortune.Font = new System.Drawing.Font("Franklin Gothic Medium", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFortune.Location = new System.Drawing.Point(446, 135);
            this.textBoxFortune.Multiline = true;
            this.textBoxFortune.Name = "textBoxFortune";
            this.textBoxFortune.Size = new System.Drawing.Size(647, 216);
            this.textBoxFortune.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(614, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 61);
            this.label1.TabIndex = 3;
            this.label1.Text = "Your Fortune:";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Franklin Gothic Medium", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(446, 391);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(647, 82);
            this.button2.TabIndex = 4;
            this.button2.Text = "Exit Fortune Teller";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 500);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxFortune);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonFortune);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFortune;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxFortune;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
    }
}

