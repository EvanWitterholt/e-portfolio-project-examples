﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LookUpGUI
{
    public partial class Form1 : Form
    {
        //Declare and initialize global constants
        const int ARRAYSIZE = 5;

        //Declare and initialize arrays
        string[] firstName   = { "Markel", "Luiza", "Bryony", "Giraldo", "Lowri" };
        string[] lastName    = { "Diggory", "Gunnar", "Hester", "Addy", "Hari" };
        string[] phoneNumber = { "555-8390", "555-4618", "555-4440", "555-1687", "555-77763" };

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            RunProgram();
        }

        private void RunProgram()
        {
            string userInput;
            string arrayFirstName;
            string arrayLastName;

            userInput = textBoxName.Text;

            for(int lcv = 0; lcv < ARRAYSIZE; ++lcv)
            {
                arrayFirstName = firstName[lcv];
                arrayLastName = lastName[lcv];

                if(textBoxName.Text == "")
                {
                    MessageBox.Show("Please Enter A Name", "NO NAME ENTERED",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    textBoxFirstName.Text = "";
                    textBoxLastName.Text = "";
                    textBoxPhone.Text = "";
                    labelNotFound.Text = "";
                    return;
                }

                if (userInput.Trim().ToUpper() == arrayFirstName.ToUpper() || 
                    userInput.Trim().ToUpper() == arrayLastName.ToUpper())
                {
                    textBoxFirstName.Text = firstName[lcv];
                    textBoxLastName.Text = lastName[lcv];
                    textBoxPhone.Text = phoneNumber[lcv];
                    labelNotFound.Text = "";
                    return;
                }
            }

            textBoxFirstName.Text = "Error";
            textBoxLastName.Text = "Error";
            textBoxPhone.Text = "Error";
            labelNotFound.Text = "USER NOT FOUND";
        }
    }
}
