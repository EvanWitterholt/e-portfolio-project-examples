﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chapter05_04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        const string END = "END";
        const int MINTEMP = -20;
        const int MAXTEMP = 130;
        const string INVALIDINPUT = "Numeric Input Between -20 And 130 Required!!!";

        int total;
        int number;
        int average;
        string userEntry;
        int userNumber;

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            bool keepGoing = true;

            userEntry = textBoxTemperature.Text;
            userEntry = userEntry.ToUpper();

            if (userEntry == END)
            {
                textBoxTemperature.Enabled = false;
                buttonEnter.Enabled = false;
                textBoxNumber.Text = number.ToString();

                if (number == 0)
                {
                    number = 1;
                }

                textBoxTotal.Text = total.ToString();
                average = total / number;
                textBoxAverage.Text = average.ToString();
                return;
            }

            if (keepGoing)
            {
                keepGoing = isNumeric(textBoxTemperature.Text);
            }

            if (keepGoing)
            {
                userNumber = Convert.ToInt32(textBoxTemperature.Text);

                if (userNumber < -20 || userNumber > 130)
                {
                    alertMessage(INVALIDINPUT, "Invalid Input");
                    textBoxNumber.Text = "";
                    textBoxTemperature.Focus();
                    return;
                }

                textBoxTemperature.Text = "";
                ++number;
                total = total + userNumber;
            }

            else
            {
                alertMessage(INVALIDINPUT, "Invalid Input");
                textBoxTemperature.Text = "";
                textBoxTemperature.Focus();
                return;
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxTemperature.Text = "";
            textBoxTemperature.Enabled = true;
            textBoxAverage.Text = "";
            textBoxNumber.Text = "";
            textBoxTotal.Text = "";
            textBoxTemperature.Focus();
            buttonEnter.Enabled = true;
            average = 0;
            total = 0;
            number = 0;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit Program Now?",
                "EXIT PROGRAM?!?",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) ==
                DialogResult.Yes)
            {
                Application.Exit();
            }

        }
    
        //-----------------------------------------------------
        private bool isNumeric(string input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        //-----------------------------------------------------
        private void alertMessage(string msg, string title)
        {
            MessageBox.Show(msg, title,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
        }
    }
}
