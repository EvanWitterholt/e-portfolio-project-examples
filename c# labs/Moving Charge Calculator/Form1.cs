﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chapter03_03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Declare and initialize global constants and variables

        const int BASERATE = 200;
        const int HOURLYRATE = 150;
        const int MILERATE = 2;
       

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            double hours = 0;
            double miles = 0;
            double charge = 0;

            hours = Convert.ToDouble(textBoxHours.Text);
            miles = Convert.ToDouble(textBoxMiles.Text);

            charge = ((BASERATE) + (hours * HOURLYRATE) + (miles * MILERATE));
            textBoxCharge.Text = charge.ToString();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxHours.Text = "";
            textBoxMiles.Text = "";
            textBoxCharge.Text = "";
            textBoxHours.Focus();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit Program Now?",
                "EXIT PROGRAM?!?",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) ==
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
